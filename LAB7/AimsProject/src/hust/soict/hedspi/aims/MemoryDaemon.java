package hust.soict.hedspi.aims;

public class MemoryDaemon {
	private long memoryUsed;
	
	public void run() {
		Runtime rt = Runtime .getRuntime();
		long used;
		
		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("\tMemory used = " + used);
				memoryUsed = used;
			}
		}
	}

	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}

}
