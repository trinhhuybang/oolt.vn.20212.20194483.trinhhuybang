package hust.soict.hedspi.test.utils;

import hust.soict.hedspi.aims.utils.MyDate;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate date1 = new MyDate();
		MyDate date2 = new MyDate(0,4,2001);
		MyDate date3 = new MyDate("January 12th 2022");
		date1.print();
		date2.print();
		date3.print();
		MyDate date4 = new MyDate();
		date4.accept();
		date4.print();
	}
}
