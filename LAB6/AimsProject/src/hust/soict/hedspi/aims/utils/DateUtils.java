package hust.soict.hedspi.aims.utils;

public class DateUtils {
	
	// Compare two dates
	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		MyDate date2 = new MyDate(3,5,2022);
		int d = compareTwoDates(date1,date2);
		if (d == 1) System.out.println(date1.getDay() + "/" + date1.getMonth() + "/" + date1.getYear() + " after " 
		+ date2.getDay() + "/" + date2.getMonth() + "/" + date2.getYear());
		else if (d == -1) System.out.println(date1.getDay() + "/" + date1.getMonth() + "/" + date1.getYear() + " before " 
				+ date2.getDay() + "/" + date2.getMonth() + "/" + date2.getYear());
		else System.out.println(date1.getDay() + "/" + date1.getMonth() + "/" + date1.getYear() + " coincides with " 
				+ date2.getDay() + "/" + date2.getMonth() + "/" + date2.getYear());
		sortDates();
		
	}
	
	
	public static int compareTwoDates(MyDate d1, MyDate d2) {
        // if d1>d2 return 1, d1<d2 return -1, d1=d2 return 0
        if (d1.getYear() > d2.getYear()) {
            return 1;
        } else if (d1.getYear() < d2.getYear()) {
            return -1;
        } else {
            if (d1.getMonth() > d2.getMonth()) {
                return 1;
            } else if (d1.getMonth() > d2.getMonth()) {
                return -1;
            } else {
                if (d1.getDay() > d2.getDay()) {
                    return 1;
                } else if (d1.getDay() < d2.getDay()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }

    // sort ASC
    public static void sortDates() {
        MyDate[] date = { new MyDate(12, 1, 2022),
                new MyDate(20, 4, 2021),
                new MyDate(17, 7, 2015),
                new MyDate(24, 8, 2001),
                new MyDate(31, 10, 2015)
        };
        MyDate tmp = new MyDate();
        boolean haveSwap = false;
        for (int i = 0; i < date.length - 1; i++) {
            haveSwap = false;
            for (int j = 0; j < date.length - i - 1; j++) {
                if (compareTwoDates(date[j], date[j + 1]) == 1) {
                    tmp = date[j];
                    date[j] = date[j + 1];
                    date[j + 1] = tmp;
                    haveSwap = true;
                }
            }
            if(haveSwap == false){
                break;
            }
        }
        System.out.println("List dates after sort:");
        for(int i = 0; i < date.length; i++){
            System.out.println(date[i].getDay() + "/" + date[i].getMonth() + "/" + date[i].getYear());
        }
    }
}
