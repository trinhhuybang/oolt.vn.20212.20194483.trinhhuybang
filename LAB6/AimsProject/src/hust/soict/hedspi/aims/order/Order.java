package hust.soict.hedspi.aims.order;

import java.util.Random;
import java.util.ArrayList;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.MyDate;

public class Order {
	// Khai bao mot hang so : so luong san pham toi da trong don hang
	public static final int MAX_NUMBERS_ORDER = 10;
	// Khai bao mot mang cac doi tuong DVD
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	// Khai bao thuoc tinh chua so luong san pham hien co trong don hang
	private int qtyOrdered = 0;
	
	private MyDate dateOrdered;
	
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	
	public Order() {
		if(this.nbOrders == MAX_LIMITTED_ORDERS)
			System.out.println("Over numbers of order!");
		else {
			this.qtyOrdered = 0;
			this.dateOrdered = new MyDate();
			nbOrders++;
		}
	}
	
	// Xay dung ca phuong thuc
	// Cac phuong thuc getter / setter
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		if (qtyOrdered >= 0)
			this.qtyOrdered = qtyOrdered;
	}
	
	public void addMedia(Media media) {
		if (!this.itemsOrdered.contains(media))
			this.itemsOrdered.add(media);
	}
	
	public void removeMedia(Media media) {
		if (this.itemsOrdered.contains(media))
			this.itemsOrdered.remove(media);
	}
	
	public float totalCost(){
		//Store the running total of the items in the order
		float total = 0;
		Media mediaItem;
		java.util.Iterator iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			total += mediaItem.getCost();
		}
		return total;
		}
	
	public Media getAluckyItem() {
		Random rd = new Random();
		int luckeyNumber = rd.nextInt(this.itemsOrdered.size());
		itemsOrdered.get(luckeyNumber).setCost(0);
		return itemsOrdered.get(luckeyNumber);
	}
	
	// Phuong thuc them mot doi tuong DVD vao don hang chinh la them mot doi tuong DVD vao mang
	// --> Phai kiem tra xem mang da day hay chua
	
	/*
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDER)
			System.out.println("The order is almost full");
		else {
			this.itemsOrdered[qtyOrdered] = disc;
			qtyOrdered ++;
			System.out.println("The disc has been added");
			System.out.println("Total disc: " + this.qtyOrdered);
		}
	}
	
	// Dau vao la mot mang cac dia
	
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDER || this.qtyOrdered + dvdList.length > MAX_NUMBERS_ORDER)
			System.out.println("The order is full !");
		else {
			for(int i = 0; i < dvdList.length; i++)
				this.addDigitalVideoDisc(dvdList[i]);
		}
	}
	
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2 ) {
		this.addDigitalVideoDisc(dvd1);
		this.addDigitalVideoDisc(dvd2);
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		//Viet lenh xu ly loai bo doi tuong khoi mang
		// Kiem tra mang khong co phan tu
		int d = 0;
		if (this.qtyOrdered == 0)
			System.out.println("The order is NULL");  // Kiem tra mang co trong khong
		else {
			for(int i = 0; i< this.qtyOrdered ; i++)
				if (this.itemsOrdered[i] == disc) {
					d = d+1;
					for(int j = i; j< this.qtyOrdered ; j++)
					this.itemsOrdered[j] = this.itemsOrdered[j + 1];
					this.qtyOrdered = this.qtyOrdered-1;
					System.out.println("The disc has been removed");
					System.out.println("Total disc: " + this.qtyOrdered);
				}
			// Kiem tra dia co trong danh muc order chua
			if (d == 0) 
				System.out.println("Array has not this disc!");
		}
	}
	// Phuong thuc tinh tong gia tri don hang
	public float totalCost(){
	float total = 0.0f;
	for(int i = 0; i< this.qtyOrdered ; i++)
	total += itemsOrdered[i].getCost();
	return total;
	}
	
	
	
	//Phuong thuc in don hang
	public void printOrder() {
		float total = 0.0f;
		System.out.println("***********************Order***********************");
		System.out.println("Date: " + dateOrdered.getDay() + "/" + dateOrdered.getMonth() + "/" + dateOrdered.getYear());
		for (int i = 0; i < this.qtyOrdered ; i++) {
			total += itemsOrdered[i].getCost();
			System.out.println(i+1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() 
					+ " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + " : " + itemsOrdered[i].getCost());
		}
		System.out.println("Total cost: " + total);
		System.out.println("***************************************************");
	}
	
	// Xay dung phuong thuc chon mot san pham
	// ngau nhien trong danh sach va mien phi cho san pham do
	public DigitalVideoDisc getAluckyItem() {
		Random rd = new Random();
		int luckeynumber = rd.nextInt(this.qtyOrdered);
		this.itemsOrdered[luckeynumber].setCost(0);
		/*int i = 0;
		for ( i = 0; i < this.qtyOrdered; i++) {
			if( i == luckeynumber) {
				this.itemsOrdered[i].setCost(0);
				break;
			}
		}
		return this.itemsOrdered[luckeynumber];
	}
	*/
}
